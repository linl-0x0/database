# Git-key配置(GitHub)

### 登陆GitHub

![image-20220423194936535](https://raw.githubusercontent.com/linl-0x0/Gallery/main/database/%E6%8A%80%E6%9C%AF%E8%B5%84%E6%96%99/Git%E7%89%88%E6%9C%AC%E6%8E%A7%E5%88%B6%E5%99%A8/202204231949921.png)

### 配置ssh-key

> 1.检查是否已存在ssh-key

```shell
$ cat ~/.ssh # 查看ssh-key是否已存在
# 存在
id_rsa           id_rsa.pub       known_hosts      known_hosts.old
# 不存在
known_hosts known_hosts.old
```

> 2.配置 ssh-keygen

```shell
$ git config --global user.name "用户名"
$ git config --global user.email "邮箱"
```

> 3.查看git config 配置

```shell
$ git config --list
credential.helper=osxkeychain
user.name=xxxxxx
user.email=xxxxxxx.com
core.repositoryformatversion=0
core.filemode=true
core.bare=false
core.logallrefupdates=true
core.ignorecase=true
core.precomposeunicode=true
remote.origin.url=git@github.com:linl-0x0/database.git
remote.origin.fetch=+refs/heads/*:refs/remotes/origin/*
branch.main.remote=origin
branch.main.merge=refs/heads/main
```

> 4.生成 ssh 私钥&&公钥对

```shell
# 运行密钥生成命令，一路回车
$ ssh-keygen
```

### 复制公钥信息

> SSH 默认私钥地址为：`~/.ssh/id_rsa` ，对应的公钥为：`~/.ssh/id_rsa.pub`，使用编辑器或 `cat` 命令输出公钥内容拷贝至剪贴板，然后将公钥粘贴至对应的配置后台。如，[Github](https://github.com/settings/keys)

```shell
$ cat ~/.ssh/id_rsa.pub
```

![image-20220423200353254](https://raw.githubusercontent.com/linl-0x0/Gallery/main/database/%E6%8A%80%E6%9C%AF%E8%B5%84%E6%96%99/Git%E7%89%88%E6%9C%AC%E6%8E%A7%E5%88%B6%E5%99%A8/202204232003373.png)

### 测试SSH是否连接成功

```shell
# 这里以 Github 服务为例
$ ssh -T git@github.com
Hi linl-0x0! You've successfully authenticated, but GitHub does not provide shell access.
```

> 这里可以看到，ssh连接GitHub，成功！！！